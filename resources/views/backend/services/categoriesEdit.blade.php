@extends('layouts.app')
@section('title')
    Cattegories | Edit #ID {{ $categories->id }}
@endsection
@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css">
@endsection
@section('javascript')
    <script>
        $(function(){
            $('input[name="name"]').on('keyup', function(){
                let Text = $(this).val();

                Text = Text.toLowerCase();
                Text = Text.replace(/[^a-zA-Z0-9]+/g,'-');

                $('input[name="slug"]').val(Text);
            })
        })
    </script>
@endsection
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Categories | Edit #ID {{ $categories->id }}</div>
                    <div class="card-body">
                        <form action="{{ route('backend.edit.process.categories', $categories->id) }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="mb-3">
                                <label for="name" class="form-global">Name <span class="text-danger">*</span></label>
                                <input type="text" name="name" value="{{ $categories->name }}"  placeholder="Name" class="form-control @error('name') is-invalid @enderror">
                                @error('name')
                                    <small class="text-danger"> {!! $message !!} </small>
                                @enderror
                            </div>

                            <div class="mb-3">
                                <label for="name" class="form-global">slug <span class="text-danger">*</span></label>
                                <input type="text" name="slug" value="{{ $categories->slug }}" placeholder="Slug" class="form-control @error('slug') is-invalid @enderror">
                                @error('slug')
                                    <small class="text-danger"> {!! $message !!} </small>
                                @enderror
                            </div>

                            <button class="btn btn-sm btn-dark">
                                Update
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
