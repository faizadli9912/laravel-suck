@extends('layouts.frontend') @section('title') Basket @endsection
@section('content')
<section class="page-section portfolio" id="portfolio">
    <div class="container">
        <!-- Portfolio Section Heading-->
        <h2 class="page-section-heading text-center text-uppercase text-secondary mb-0">Basket</h2>
        <!-- Icon Divider-->
        <div class="divider-custom">
            <div class="divider-custom-line"></div>
            <div class="divider-custom-icon">
                <i class="fas fa-star"></i>
            </div>
            <div class="divider-custom-line"></div>
        </div>
        <!-- Portfolio Grid Items-->
        <div class="row justify-content-center">
            @if (Session::has('success'))
            <div class="alert alert-success">{{ Session::get('success') }}</div>
            @endif @if (Cart::isEmpty())
            <span class="text-muted text-center">Your basket is Empty!</span>
            @else
            <form action="{{ route('frontend.basket.update') }}" method="post">
                @csrf @foreach (Cart::getContent() as $key => $item)
                <input type="hidden" name="id[]" value="{{ $item['id'] }}">
                <div class="bg-secondary-custom p-3 row">
                    <div class="col-2">
                        <img
                            src="{{ asset('services/'.$item['attributes']['image'])}}"
                            width="100"
                            class="rounded-circle">
                    </div>
                    <div class="col-5 bg-secondary-custom p-3">{{ $item['name'] }}</div>
                    <div class="col-2 bg-secondary-custom p-3">
                        <input
                            type="number"
                            name="quantity"
                            value="{{ $item['quantity'] }}"
                            class="form-control">
                    </div>
                    <div class="col-2 bg-secondary-custom p-3 text-end">{!! number_format($item['price'], 0, '.', ',') !!}</div>
                    <div class="col-2 bg-secondary-custom p-3 text-end">{!! number_format($item['price']*$item['quantity'], 0, '.', ',') !!}</div>
                    <div class="col-1 p-3 d-flex justify-content-center align-items-center">
                        <a href="{{ route('frontend.basket.remove', $item['id']) }}">
                            <i class="fas fa-times text-danger"></i>
                        </a>
                    </div>
                </div>

                @endforeach
            </form>

            <div class="row mb-3">
                <div class="col-2"></div>
                <div class="col-5"></div>
                <div class="col-2 p-3 text-end"> <button type="submit" class="btn btn-primary w-100">Update Quantity</button> </div>
                <div class="col-1 p-3 d-flex justify-content-center align-items-center"></div>
            </div>

            <div class="row mb-3">
                <div class="col-2"></div>
                <div class="col-5"></div>
                <div class="col-2 p-3">Sub-Total :</div>
                <div class="col-2 p-3 text-end">{!! number_format(Cart::getSubTotal(), 0,'.',',') !!}</div>
                <div class="col-1 p-3 d-flex justify-content-center align-items-center"></div>
            </div>

            <div class="row mb-3">
                <div class="col-2"></div>
                <div class="col-5"></div>
                <div class="col-2 p-3">PPN 11% :</div>
                <div class="col-2 p-3 text-end">{!! number_format(Cart::getSubTotal()*0.11, 0,'.',',') !!}</div>
                <div class="col-1 p-3 d-flex justify-content-center align-items-center"></div>
            </div>

            <div class="row mb-3">
                <div class="col-2"></div>
                <div class="col-5"></div>
                <div class="col-2 p-3">Total :</div>
                <div class="col-2 p-3 text-end">{!! number_format(Cart::getSubTotal()*0.11, 0,'.',',') !!}</div>
                <div class="col-1 p-3 d-flex justify-content-center align-items-center"></div>
            </div>

            @endif
        </div>
    </div>
</section>

@endsection @section('css')
<style>
    .page-section {
        padding: 12rem 0 2rem 0 !important;
    }

    .bg-secondary-custom {
        background-color: #ecf0f1;
        border: 1px solid #eeeeee;
    }
</style>
@endsection