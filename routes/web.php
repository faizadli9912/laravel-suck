<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Frontend\HomeController;
use App\Http\Controllers\Frontend\AboutController;
use App\Http\Controllers\Frontend\ContactController;
use App\Http\Controllers\Frontend\PortfolioController;
use App\Http\Controllers\Frontend\ServicesController;
use App\Http\Controllers\Frontend\BasketController;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Backend\HomeController as BackendHomeController;
use App\Http\Controllers\Backend\AboutController as BackendAboutController;
use App\Http\Controllers\Backend\CvController as BackendCvController;
use App\Http\Controllers\Backend\ContactController as BackendContactController;
use App\Http\Controllers\Backend\PortfolioController as BackendPortfolioController;
use App\Http\Controllers\Backend\FooterController as BackendFooterController;
use App\Http\Controllers\Backend\ProfileController as BackendProfileController;
use App\Http\Controllers\Backend\Services\ServiceCategoriesController as BackendServiceCategoriesController;
use App\Http\Controllers\Backend\Services\ServiceItemsController as BackendServiceItemsController;

Route::get('/', [HomeController::class, 'index'])->name("frontend.home.index");
Route::get('/portfolio', [PortfolioController::class, 'index'])->name("frontend.portfolio.index");
Route::get('/service', [ServicesController::class, 'index'])->name("frontend.services.index");

Route::get('/basket', [BasketController::class, 'index'])->name("frontend.basket.index");
Route::post('/add-to-cart', [BasketController::class, 'add_to_cart'])->name("frontend.basket.add");
Route::get('/remove-cart/{id?}', [BasketController::class, 'remove_cart'])->name("frontend.basket.remove");
Route::post('/update-cart', [BasketController::class, 'update_cart'])->name("frontend.basket.update");

Route::get('/about', [AboutController::class, 'index'])->name("frontend.about.index");
Route::get('/download/cv', [AboutController::class, 'download_cv'])->name("frontend.about.download.my.cv");
Route::get('/contact', [ContactController::class, 'index'])->name("frontend.contact.index")->middleware('is_admin');
Route::post('/contact/process', [ContactController::class, 'process'])->name("frontend.contact.process");
Route::get('/backend/manage/home', [BackendHomeController::class, 'index'])->name("frontend.manage.home");

Route::get('/backend/manage/portfolio', [BackendPortfolioController::class, 'index'])->name('backend.manage.portfolio');
Route::get('/backend/create/portfolio', [BackendPortfolioController::class, 'create'])->name('backend.create.portfolio');
Route::post('/backend/create/process/portfolio', [BackendPortfolioController::class, 'create_process'])->name('backend.create.process.portfolio');
Route::get('/backend/show/portfolio/{portfolio}', [BackendPortfolioController::class, 'show'])->name('backend.show.portfolio');
Route::get('/backend/edit/portfolio/{portfolio}', [BackendPortfolioController::class, 'edit'])->name('backend.edit.portfolio');
Route::post('/backend/edit/process/{portfolio}', [BackendPortfolioController::class, 'edit_process'])->name('backend.edit.process.portfolio');
Route::delete('/backend/delete/{portfolio}', [BackendPortfolioController::class, 'destroy'])->name('backend.delete.portfolio');

Route::get('/backend/services/categories', [BackendServiceCategoriesController::class, 'index'])->name('backend.services.categories');
Route::get('/backend/services/categories/show/{categories}', [BackendServiceCategoriesController::class, 'show'])->name('backend.categories.show');
Route::get('/backend/services/categories/create', [BackendServiceCategoriesController::class, 'create'])->name('backend.services.create.categories');
Route::post('/backend/services/categories/create', [BackendServiceCategoriesController::class, 'store'])->name('backend.categories.store');
Route::get('/backend/services/categories/edit/{categories}', [BackendServiceCategoriesController::class, 'edit'])->name('backend.categories.edit');
Route::post('/backend/services/categories/edit/process/{categories}', [BackendServiceCategoriesController::class, 'edit_process'])->name('backend.edit.process.categories');
Route::delete('/backend/services/destroy/{categories}', [BackendServiceCategoriesController::class, 'destroy'])->name('backend.categories.destroy');

Route::get('/backend/services/items', [BackendServiceItemsController::class, 'index'])->name('backend.services.items');
Route::get('/backend/services/items/show/{items}', [BackendServiceItemsController::class, 'show'])->name('backend.items.show');
Route::get('/backend/services/items/create', [BackendServiceItemsController::class, 'create'])->name('backend.services.create.items');
Route::post('/backend/services/items/create', [BackendServiceItemsController::class, 'store'])->name('backend.items.store');
Route::get('/backend/services/items/edit/{items}', [BackendServiceItemsController::class, 'edit'])->name('backend.items.edit');
Route::post('/backend/services/items/edit/process/{items}', [BackendServiceItemsController::class, 'edit_process'])->name('backend.edit.process.items');
Route::delete('/backend/services/destroy/{items}', [BackendServiceItemsController::class, 'destroy'])->name('backend.items.destroy');

Route::get('/backend/manage/about', [BackendAboutController::class, 'index'])->name("backend.manage.about");
Route::post('/backend/manage/about/process', [BackendAboutController::class, 'process'])->name("backend.manage.about.process");

Route::get('/backend/manage/cv', [BackendCvController::class, 'index'])->name("backend.manage.cv");
Route::post('/backend/manage/cv/process', [BackendCvController::class, 'process'])->name("backend.manage.cv.process");

Route::get('/backend/manage/contact', [BackendContactController::class, 'index'])->name("backend.manage.contact")->middleware('is_admin');
Route::get('/backend/manage/footer', [BackendFooterController::class, 'index'])->name("backend.manage.footer");
Route::get('/backend/edit/footer/{$id}', [BackendFooterController::class, 'edit'])->name("backend.edit.footer");
Route::post('/backend/edit/footer/process', [BackendFooterController::class, 'edit_process'])->name("backend.edit.process.footer");
Route::get('/backend/profile', [BackendProfileController::class, 'profile'])->name("backend.profile");
Route::post('/backend/profile/process', [BackendProfileController::class, 'profile_process'])->name("backend.profile.process");

Route::get('/error-access-admin', function(){
    return view('error-access-admin');
})->name("error.admin.access");

Auth::routes([
    'register' => false,
    'reset' => false,
    'verify' => false,
]);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
