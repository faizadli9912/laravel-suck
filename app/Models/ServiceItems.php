<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServiceItems extends Model
{
    use HasFactory;

    protected $fillable = [
        'service_category_id',
        'name',
        'slug',
        'description',
        'image',
        'price',
        'total_days',
    ];

    public function service_category()
    {
        return $this->belongsTo(ServiceCategories::class);
    }
}
