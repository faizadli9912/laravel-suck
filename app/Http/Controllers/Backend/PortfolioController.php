<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Portfolio;
use Illuminate\Http\Request;

class PortfolioController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($id = null)
    {

        // $show_portfolio = Portfolio::where('id', $id)->get();
        // $on_show = false;
        $portfolio = Portfolio::get();
        return view('backend.portfolio.index', compact('portfolio'));
    }

    public function show($id)
    {
        if ($id == null) {
            return redirect()->route('backend.manage.portfolio')->with('error', 'The ID is empty!');
        } else {
            $portfolio = Portfolio::find($id);

            if ($portfolio) {
                return view('backend.portfolio.show', compact('portfolio'));
            } else {
                return redirect()->route('backend.manage.portfolio')->with('error', "The #ID {$id} not found in Database!");
            }
        }
    }

    public function create()
    {
        return view('backend.portfolio.create');
    }

    public function edit($id)
    {
        if ($id == null) {
            return redirect()->route('backend.manage.portfolio')->with('error', 'The ID is empty!');
        } else {
            $portfolio = Portfolio::find($id);

            if ($portfolio) {
                return view('backend.portfolio.edit', compact('portfolio'));
            } else {
                return redirect()->route('backend.manage.portfolio')->with('error', "The #ID {$id} not found in Database!");
            }
        }
    }

    public function edit_process(Request $request)
    {
        request()->validate([
            'title'         => 'required',
            'image'         => 'required|max:2048|mimes:jpg,jpeg,png',
            'description'   => 'required'
        ]);

        $portfolio = Portfolio::find($request->id);

        if (public_path('portoflio/'. $portfolio->image))
            unlink(public_path('portofolio/'.$portfolio->image));

        $image = time() . '.' . $request->image->extension();
        $request->image->move(public_path('portofolio'), $image);


        Portfolio::where('id', $request->id)
            ->update(([
                'title'         => $request->title,
                'image'         => $image,
                'description'   => $request->description
            ]));

        return redirect()->route('backend.manage.portfolio')->with('success', 'Item Edited Successfully');

    }


    public function create_process(Request $request)
    {
        request()->validate([
            'title'         => 'required',
            'image'         => 'required|max:2048|mimes:jpg,jpeg,png',
            'description'   => 'required'
        ]);

        $image = time() . '.' . $request->image->extension();
        $request->image->move(public_path('portofolio'), $image);


        portfolio::create([
            'title'         => $request->title,
            'image'         => $image,
            'description'   => $request->description
        ]);

        return redirect()
            ->route('backend.manage.portfolio')
            ->with('success', 'Item Created Successfully');

    }

    public function destroy($id)
    {
        $portfolio = Portfolio::find($id);

        if (public_path('portoflio/'. $portfolio->image))
            unlink(public_path('portofolio/'.$portfolio->image));

        $portfolio->delete();

        return redirect()->route('backend.manage.portfolio')->with('success', 'Item Deleted Successfully');
    }
}
