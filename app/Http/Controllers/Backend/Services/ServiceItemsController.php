<?php

namespace App\Http\Controllers\Backend\Services;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\ServiceItems;

class ServiceItemsController extends Controller
{
    public function index()
    {
        $items = ServiceItems::orderBy('name')->get();
        return view('backend.services.items', compact('items'));
    }

    public function create()
    {
        return view('backend.services.itemsCreate');
    }

    public function show(ServiceItems $items)
    {
        $title = 'Category Show #' . $items->id;
        return view('backend.services.itemsShow', compact('items', 'title'));
    }

    public function store(Request $request)
    {
        $rule = [
            'name' => 'required',
            'slug' => 'required',
        ];

        $messages = [
            'name.required' => 'The field <strong>name</strong> is required!',
            'slug.required' => 'The field <strong>name</strong> is required!',
        ];

        $validator = Validator::make($request->all(), $rule, $messages);

        if ($validator->fails()) {
            return redirect()->route('backend.services.itemsCreate')->withErrors($validator)->withInput();
        } else {
            ServiceItems::create($request->all());
            return redirect()->route('backend.services.items')->with('succes', "The Category <strong>{$request->name}</strong> created successfully");
        }
    }

    public function edit($id)
    {
        if ($id == null) {
            return redirect()->route('backend.services.items')->with('error', 'The ID is empty!');
        } else {
            $items = ServiceItems::find($id);

            if ($items) {
                return view('backend.services.itemsEdit', compact('items'));
            } else {
                return redirect()->route('backend.services.items')->with('error', "The #ID {$id} not found in Database!");
            }
        }
    }

    public function edit_process(Request $request, ServiceItems $categories)
    {
        $rule = [
            'name' => 'requirxed',
            'slug' => 'required',
        ];

        $messages = [
            'name.required' => 'The field <strong>name</strong> is required!',
            'slug.required' => 'The field <strong>name</strong> is required!',
        ];

        $validator = Validator::make($request->all(), $rule, $messages);

        if ($validator->fails()) {
            return redirect()->route('backend.services.categoriesEdit', $categories->id)->withErrors($validator)->withInput();
        } else {
            $categories->update($request->all());
            return redirect()->route('backend.services.categories')
                             ->with('success', "The Category <strong>{$request->name}</strong> updated successfully");
        }

    }

    public function destroy($id)
    {
        $items = ServiceItems::find($id);

        $items->delete();

        return redirect()->route('backend.services.items')->with('success', "The Category <strong>{$items->name}</strong> deleted successfully");
    }
}
