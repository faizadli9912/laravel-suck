<?php

namespace App\Http\Controllers\Backend\Services;

use App\Http\Controllers\Controller;
use App\Models\ServiceCategories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class ServiceCategoriesController extends Controller
{
    public function index()
    {
        $categories = ServiceCategories::orderBy('name')->get();
        return view('backend.services.categories', compact('categories'));
    }

    public function create()
    {
        return view('backend.services.categoriesCreate');
    }

    public function show(ServiceCategories $categories)
    {
        $title = 'Category Show #' . $categories->id;
        return view('backend.services.categoriesShow', compact('categories', 'title'));
    }

    public function store(Request $request)
    {
        $rule = [
            'name' => 'required',
            'slug' => 'required',
        ];

        $messages = [
            'name.required' => 'The field <strong>name</strong> is required!',
            'slug.required' => 'The field <strong>name</strong> is required!',
        ];

        $validator = Validator::make($request->all(), $rule, $messages);

        if ($validator->fails()) {
            return redirect()->route('backend.services.categoriesCreate')->withErrors($validator)->withInput();
        } else {
            ServiceCategories::create($request->all());
            return redirect()->route('backend.services.categories')->with('succes', "The Category <strong>{$request->name}</strong> created successfully");
        }
    }

    public function edit($id)
    {
        if ($id == null) {
            return redirect()->route('backend.services.categories')->with('error', 'The ID is empty!');
        } else {
            $categories = ServiceCategories::find($id);

            if ($categories) {
                return view('backend.services.categoriesEdit', compact('categories'));
            } else {
                return redirect()->route('backend.services.categories')->with('error', "The #ID {$id} not found in Database!");
            }
        }
    }

    public function edit_process(Request $request, ServiceCategories $categories)
    {
        $rule = [
            'name' => 'required',
            'slug' => 'required',
        ];

        $messages = [
            'name.required' => 'The field <strong>name</strong> is required!',
            'slug.required' => 'The field <strong>name</strong> is required!',
        ];

        $validator = Validator::make($request->all(), $rule, $messages);

        if ($validator->fails()) {
            return redirect()->route('backend.services.categoriesEdit', $categories->id)->withErrors($validator)->withInput();
        } else {
            $categories->update($request->all());
            return redirect()->route('backend.services.categories')
                             ->with('success', "The Category <strong>{$request->name}</strong> updated successfully");
        }

    }

    public function destroy($id)
    {
        $category = ServiceCategories::find($id);

        $category->delete();

        return redirect()->route('backend.services.categories')->with('success', "The Category <strong>{$category->name}</strong> deleted successfully");
    }
}
