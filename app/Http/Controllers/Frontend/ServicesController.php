<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\ServiceItems;
use Illuminate\Http\Request;

class ServicesController extends Controller
{
    public function index()
    {
        $service_items = ServiceItems::orderBy('name', 'asc')->get();
        return view('frontend.services.index', compact('service_items'));
    }
}
