<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Cart;

class BasketController extends Controller
{
    public function index()
    {
        return view('frontend.basket.index');
    }

    public function add_to_cart(Request $request)
    {
        Cart::add([
            'id' => $request->id,
            'name' => $request->name,
            'price' => $request->price,
            'quantity' => $request->quantity,
            'attributes' => [
                'image' => $request->image,
            ]
        ]);

        return redirect()->route('frontend.basket.index')->with('success', 'The item added successfully');
    }


    public function remove_cart($id)
    {
        Cart::remove($id);

        return redirect()->route('frontend.basket.index')->with('success', 'The item deleted successfully');
    }

    public function update_cart(Request $request) 
    {
        if ($request) {
            $id = $request->id;
            $quantity = $request->quantity;

            for ($i=0; $i < count($id) ; $i++) { 
                Cart::update($id[$i], [
                    'quantity' => [
                        //
                        'relative' => false,
                        'value' => $quantity[$i]
                    ]
                ]);
            }
        }
    }
}
